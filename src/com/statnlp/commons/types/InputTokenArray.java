/** Statistical Natural Language Processing System
    Copyright (C) 2014-2015  Lu, Wei

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.statnlp.commons.types;

import java.io.Serializable;
import java.util.Arrays;

public class InputTokenArray implements Serializable{
	
	private static final long serialVersionUID = 8181520592866135065L;
	
	protected InputToken[] _tokens;
	
	public InputTokenArray(InputToken[] tokens){
		this._tokens = tokens;
	}
	
	public InputToken[] getTokens(){
		return this._tokens;
	}
	
	public int size(){
		return this._tokens.length;
	}
	
	@Override
	public boolean equals(Object o){
		if(o instanceof InputTokenArray){
			InputTokenArray a = (InputTokenArray)o;
			return Arrays.equals(this._tokens, a._tokens);
		}
		return false;
	}
	
	@Override
	public int hashCode(){
		return Arrays.hashCode(this._tokens);
	}
	
	@Override
	public String toString(){
		return "InputTokenArray:"+Arrays.toString(this._tokens);
	}
	
}